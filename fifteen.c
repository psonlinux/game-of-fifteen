/**
 * fifteen.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Implements the Game of Fifteen (generalized to d x d).
 *
 * Usage: ./fifteen d
 *
 * whereby the board's dimensions are to be d x d,
 * where d must be in [MIN,MAX]
 *
 * Note that usleep is obsolete, but it offers more granularity than
 * sleep and is simpler to use than nanosleep; `man usleep` for more.
 */
 
#define _XOPEN_SOURCE 500

#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// board's minimal dimension
#define MIN 3

// board's maximal dimension
#define MAX 9

// board, whereby board[i][j] represents row i and column j
int board[MAX][MAX];

// board's dimension
int d;

//co-ordinate of blank tile.'_' is used for blank tile.
int bx = -1,by = -1;

// prototypes
void clear(void);
void greet(void);
void init(void);
void draw(void);
bool move(int tile);
bool won(void);
void save(void);

int main(int argc, string argv[])
{
    // greet player
    greet();

    // ensure proper usage
    if (argc != 2)
    {
        printf("Usage: ./fifteen d\n");
        return 1;
    }

    // ensure valid dimensions
    d = atoi(argv[1]);
    if (d < MIN || d > MAX)
    {
        printf("Board must be between %i x %i and %i x %i, inclusive.\n",
            MIN, MIN, MAX, MAX);
        return 2;
    }

    // initialize the board
    init();

    // accept moves until game is won
    while (true)
    {
        // clear the screen
        clear();

        // draw the current state of the board
        draw();

        // saves the current state of the board (for testing)
        save();

        // check for win
        if (won())
        {
            printf("YOU WIN!\n");
            break;
        }

        // prompt for move
        printf("Tile to move: ");
        int tile;
        scanf("%d", &tile);

        // move if possible, else report illegality
        if (!move(tile))
        {
            printf("\nIllegal move.\n");
            usleep(300000);
        }

        // sleep for animation's sake
        usleep(300000);
    }

    // that's all folks
    return 0;
}

/**
 * Clears screen using ANSI escape sequences.
 */
void clear(void)
{
    //clear the screen.
    printf("\033[2J");
    
    //puts the cursor at line 0 and column 0.
    printf("\033[%d;%dH", 0, 0);
}

/**
 * Greets player.
 */
void greet(void)
{
    clear();
    printf("GAME OF FIFTEEN\n");
    usleep(1000000);
}

/**
 * Initializes the game's board with tiles numbered 1 through d*d - 1,
 * (i.e., fills board with values but does not actually print them),
 * whereby board[i][j] represents row i and column j.
 */
void init(void)
{   
    bx=d-1;
    by=d-1;
    
    //Assumption:No two tile should have same number on it.
    for(int row=0; row < d; row++){
        for(int col=0; col < d; col++){
            if(row == d-1 && col == d-1){
                board[row][col]='_';
            }
            else{                
                printf("board[%d][%d]=",row,col);
                scanf("%d",&board[row][col]);
            }
        }
    }
   
}

/**
 * Prints the board in its current state.
 */
void draw(void)
{
    for(int row=0; row < d; row++){
        for(int col=0; col < d; col++){
            if(board[row][col] == '_' )
                printf("%-4c",board[row][col]);
            else
                //prints tile in green color
                printf("\033[32;01m%-4d",board[row][col]);                
            
        }
        printf("\n\n");
    }
    
    //reset the fg color
    printf("\033[00m");
}

//swap given tile with blank tile and udates co-ordinate of blank tile.
void swapAndUpdate(int tile_x,int tile_y){
    int temp=0;
    
    temp = board[tile_x][tile_y];
    board[tile_x][tile_y]  = board[bx][by];
    board[bx][by]  = temp;
    
    //update bx,by
    bx = tile_x;
    by = tile_y;
}

/**
 * If tile borders empty space, moves tile and returns true, else
 * returns false. 
 */
bool move(int tile)
{  
    //co-ordinate of tile.
    int tile_x=-1,tile_y=-1;
    
    for(int i=0; i < d; i++){
        for(int j=0; j<d; j++){
            if(board[i][j] == tile){
                tile_x = i;
                tile_y = j;
            }
        }
        //break from loop if tile found on board.
        if(tile_x >= 0)
            break;
    }
    
    if(tile_x == -1)
        return false;
    
    //check for the presence of blank tile adjacent to given tile.
    //if blank tile found then swap with it and update the co-ordinate
    //of blank tile.
    //a tile move to four possible location.
    
    if( tile_x == bx && tile_y + 1 == by){
        swapAndUpdate(tile_x,tile_y);            
        return true;
    }
    else if(tile_x == bx && tile_y - 1== by){
        swapAndUpdate(tile_x,tile_y);
        return true;
    }
    else if(tile_x - 1 == bx && tile_y == by){
        swapAndUpdate(tile_x,tile_y);
        return true;
    }
    else if(tile_x + 1 == bx && tile_y == by){
        swapAndUpdate(tile_x,tile_y);
        return true;
    }
    else
        return false; 
}

/**
 * Returns true if game is won (i.e., board is in winning configuration), 
 * else false.
 */
bool won(void)
{   
    //stores current largest num while looping
    int currentLargestNum = board[0][0];   
        
    //keep comparing each num with largest number we hv seen so far
    //if the num is equal or greater than currentLargestNum than make it currentLargestNum
    //else return false 
    for(int row=0; row < d; row++){
        for(int col=0; col < d; col++){
            if(currentLargestNum > board[row][col])
                return false;
            else
                currentLargestNum = board[row][col] ;
        }
    }
    return true;
}

/**
 * Saves the current state of the board to disk (for testing).
 */
void save(void)
{
    // log
    const string log = "log.txt";

    // delete existing log, if any, before first save
    static bool saved = false;
    if (!saved)
    {
        unlink(log);
        saved = true;
    }

    // open log
    FILE* p = fopen(log, "a");
    if (p == NULL)
    {
        return;
    }

    // log board
    fprintf(p, "{");
    for (int i = 0; i < d; i++)
    {
        fprintf(p, "{");
        for (int j = 0; j < d; j++)
        {
            fprintf(p, "%i", board[i][j]);
            if (j < d - 1)
            {
                fprintf(p, ",");
            }
        }
        fprintf(p, "}");
        if (i < d - 1)
        {
            fprintf(p, ",");
        }
    }
    fprintf(p, "}\n");

    // close log
    fclose(p);
}
